#!/bin/bash

rootDir=$(dirname $BASH_SOURCE)
# echo $rootDir
toolsDir=$(realpath $rootDir/tools)
echo "Add to PATH: $toolsDir"
export PATH=$PATH:$toolsDir
