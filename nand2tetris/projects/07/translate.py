import sys

class CommandType:
    C_ARITHMETIC = 'C_ARITHMETIC'
    C_PUSH = 'C_PUSH'
    C_POP = 'C_POP'
    C_LABEL = 'C_LABEL'
    C_GOTO = 'C_GOTO'
    C_IF = 'C_IF'
    C_FUNCTION = 'C_FUNCTION'
    C_RETURN = 'C_RETURN'
    C_CALL = 'C_CALL'

    C_TYPES = [
        C_ARITHMETIC,
        C_PUSH,
        C_POP,
        C_LABEL,
        C_GOTO,
        C_IF,
        C_FUNCTION,
        C_RETURN,
        C_CALL,
    ]

    VM_ADD = 'add'
    VM_SUB = 'sub'
    VM_NEG = 'neg'
    VM_EQ = 'eq'
    VM_GT = 'gt'
    VM_LT = 'lt'
    VM_AND = 'and'
    VM_OR = 'or'
    VM_NOT = 'not'
    VM_PUSH = 'push'
    VM_POP = 'pop'

    VM_CMDS = [
        VM_ADD,
        VM_SUB,
        VM_NEG,
        VM_EQ,
        VM_GT,
        VM_LT,
        VM_AND,
        VM_OR,
        VM_NOT,
        VM_PUSH,
        VM_POP,
    ]

    C_TYPE_MAP = {
        VM_ADD: C_ARITHMETIC,
        VM_SUB: C_ARITHMETIC,
        VM_NEG: C_ARITHMETIC,
        VM_EQ: C_ARITHMETIC,
        VM_GT: C_ARITHMETIC,
        VM_LT: C_ARITHMETIC,
        VM_AND: C_ARITHMETIC,
        VM_OR: C_ARITHMETIC,
        VM_NOT: C_ARITHMETIC,
        VM_PUSH: C_PUSH,
        VM_POP: C_POP,
    }

    BINARY_ARITHMETIC_MAP = {
        VM_ADD: '+',
        VM_SUB: '-',
        VM_AND: '&',
        VM_OR: '|',
    }

    UNARY_ARITHMETIC_MAP = {
        VM_NEG: '-',
        VM_NOT: '!',
    }

    COMPARE_ARITHMETIC_MAP = {
        VM_EQ: 'EQ',
        VM_GT: 'GT',
        VM_LT: 'LT',
    }

    SEGMENT_CONSTANT = 'constant'
    SEGMENT_LOCAL = 'local'
    SEGMENT_ARGUMENT = 'argument'
    SEGMENT_THIS = 'this'
    SEGMENT_THAT = 'that'
    SEGMENT_TEMP = 'temp'
    SEGMENT_POINTER = 'pointer'
    SEGMENT_STATIC = 'static'

    SEGMENT_MAP = {
        SEGMENT_CONSTANT: 'CONSTANT',
        SEGMENT_LOCAL: 'LCL',
        SEGMENT_ARGUMENT: 'ARG',
        SEGMENT_THIS: 'THIS',
        SEGMENT_THAT: 'THAT',
        SEGMENT_TEMP: 'TEMP',
        SEGMENT_POINTER: 'POINTER',
        SEGMENT_STATIC: 'STATIC',
    }

class Line:
    def __init__(self, line):
        self.tokens = line.split()
        self.op = self.tokens[0]
        token_length = len(self.tokens)
        self.extra1 = self.tokens[1] if token_length > 1 else None
        self.extra2 = self.tokens[2] if token_length == 3 else None
        self.command_type = CommandType.C_TYPE_MAP[self.op]
        if self.command_type == CommandType.C_ARITHMETIC:
            self.arg1 = self.op
        else:
            self.arg1 = self.extra1
        if self.command_type in (CommandType.C_PUSH, CommandType.C_POP):
            self.arg2 = self.extra2
        else:
            self.arg2 = None

    def __str__(self):
        return '<{}: {}, {}>'.format(
            self.command_type,
            # self.extra1, self.extra2,
            self.arg1, self.arg2
        )


class Parser:
    def __init__(self, filename):
        self.filename = filename
        self.raw_lines = None
        self.line_tokens = []
        self.lines = None
        with open(filename, 'r') as f:
            self.raw_lines = [x.strip().split('//')[0] for x in f]
            self.raw_lines = [x for x in self.raw_lines if x]
            self.lines = [Line(x) for x in self.raw_lines]


class CodeWriter:
    def __init__(self, parser):
        self.cmp_count = 0
        self.parser = parser
        for l in self.parser.lines:
            print('//', l)

        print()
        self.generate_preamble()
        for l in self.parser.lines:
            self.generate_line_assembly(l)

    def generate_preamble(self):
        template = '''\
            // Initialize SP.
            // @20
            @256
            D=A
            @SP
            M=D
        '''
        output = self.sanitize_asm_string(template)
        print(output)

    def generate_line_assembly(self, line):
        if line.command_type == CommandType.C_PUSH:
            self.generate_push(line)
        elif line.command_type == CommandType.C_POP:
            self.generate_pop(line)
        elif line.command_type == CommandType.C_ARITHMETIC:
            self.generate_arithmetic(line)

    @staticmethod
    def sanitize_asm_string(input):
        return '\n'.join([x.strip() for x in input.split('\n')])

    @staticmethod
    def push_pop_segment_config(push_pop_segment_id):
        if push_pop_segment_id == CommandType.SEGMENT_TEMP:
            segment = 5
            extra = ''
        elif push_pop_segment_id == CommandType.SEGMENT_POINTER:
            segment = 3
            extra = ''
        elif push_pop_segment_id == CommandType.SEGMENT_STATIC:
            segment = 16
            extra = ''
        else:
            segment = CommandType.SEGMENT_MAP[push_pop_segment_id]
            extra = 'A=M'
        return (segment, extra)

    def generate_push(self, line):
        push_d_template = '''\
            @SP // Load SP into A register.
            A=M
            M=D // Push D onto stack.
            @SP // SP++.
            M=M+1
        '''

        if line.arg1 == CommandType.SEGMENT_CONSTANT:
            template = '''\
                // PUSH {segment} {val}
                @{val}
                D=A // Place {val} in D register.
            '''.format(**{'segment': CommandType.SEGMENT_MAP[line.arg1], 'val': line.arg2})
            output = self.sanitize_asm_string(template + push_d_template)
        else:
            segment, extra = self.push_pop_segment_config(line.arg1)
            template = '''\
                // PUSH {segment} {offset}
                @{offset}
                D=A // {offset} -> D.
                @{segment}
                {extra}
                A=D+A // {segment} + {offset} -> A.
                D=M
            '''.format(**{'segment': segment, 'offset': line.arg2, 'extra': extra})
            output = self.sanitize_asm_string(template + push_d_template)
        print(output)

    def generate_pop(self, line):
        segment, extra = self.push_pop_segment_config(line.arg1)
        template = '''\
            // POP {segment} {offset}
            @{offset}
            D=A
            @{segment}
            {extra}
            D=A+D // segment + offset -> D.
            @R15
            M=D // segment + offset -> R15.
            @SP // SP--.
            AM=M-1
            D=M // pop D.
            @R15
            A=M
            M=D
        '''.format(**{'segment': segment, 'offset': line.arg2, 'extra': extra})
        output = self.sanitize_asm_string(template)
        print(output)

    def generate_arithmetic(self, line):
        if line.arg1 in CommandType.BINARY_ARITHMETIC_MAP.keys():
            template = '''\
                // {command}
                @SP // SP--.
                AM=M-1 // Load SP into A.
                D=M // Pop stack into D.
                @SP // SP--.
                AM=M-1 // Load SP into A.
                M=M{operator}D // Perform operation: {command}.
                @SP // SP++.
                M=M+1
            '''.format(**{'command': line.arg1, 'operator': CommandType.BINARY_ARITHMETIC_MAP[line.arg1]})
            output = self.sanitize_asm_string(template)
            print(output)
        elif line.arg1 in CommandType.UNARY_ARITHMETIC_MAP.keys():
            template = '''\
                // {command}
                @SP // SP--.
                AM=M-1 // Load SP into A.
                M={operator}M // Perform operation: {command}.
                @SP // SP++.
                M=M+1
            '''.format(**{'command': line.arg1, 'operator': CommandType.UNARY_ARITHMETIC_MAP[line.arg1]})
            output = self.sanitize_asm_string(template)
            print(output)
        elif line.arg1 in CommandType.COMPARE_ARITHMETIC_MAP.keys():
            template = '''\
                // {command}
                @SP // SP--.
                AM=M-1 // Load SP into A.
                D=M // Pop stack into D.
                @SP // SP--.
                AM=M-1 // Load SP into A.
                D=M-D // Perform initial subtraction.

                @GOOD_{count}
                D;J{operator}

                (BAD_{count})
                @0
                D=A
                @DONE_{count}
                0;JMP

                (GOOD_{count})
                @0
                D=A
                D=D-1
                @DONE_{count}
                0;JMP

                (DONE_{count})
                @SP
                A=M
                M=D
                @SP // SP++.
                M=M+1
            '''.format(**{
                'command': line.arg1,
                'operator': CommandType.COMPARE_ARITHMETIC_MAP[line.arg1],
                'count': self.cmp_count,
            })
            output = self.sanitize_asm_string(template)
            print(output)
            self.cmp_count += 1


class Runner:
    def __init__(self, filename):
        self.filename = filename
        self.parser = Parser(filename)
        self.code_writer = CodeWriter(self.parser)

if __name__ == '__main__':
    r = Runner(sys.argv[1])
