// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

@SCREEN
D=A
@address
M=D

@8192
D=A
@address
A=M
D=D+A
@limit
M=D

@value
M=0

(resetSystem)
@SCREEN
D=A
@address
M=D

@value
M=0

@KBD
D=M

@endReset
D;JEQ
@value
M=-1

(endReset)
@loop
0;JMP

(loop)
@value
D=M
@address
A=M
M=D
@address
M=M+1

@limit
D=M
@address
D=D-M
@resetSystem
D;JEQ

@loop
0;JMP

(END)
@END
0;JMP
