// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

@R0
D=M
@reg0
M=D

@R1
D=M
@reg1
M=D

@reg0
D=M
@reg1
D=D-M
@RUN
D;JGE

// Swap 'reg0' <-> 'reg1'.
// Store 'reg0' in 'temp'.
@reg0
D=M
@temp
M=D

// 'reg1' -> 'reg0'.
@reg1
D=M
@reg0
M=D

// 'temp' -> 'reg1'.
@temp
D=M
@reg1
M=D

(RUN)
// Initialize @R2.
@R2
M=0

// Adder amount.
@reg0
D=M
@adder
M=D

// Store original value in D.
@reg1
D=M

// Assign counter limit.
@count
M=D

@value
M=0

(LOOP)

@count
D=M
@FINISH
D;JEQ

@adder
D=M
@value
M=M+D

@count
M=M-1

@LOOP
0;JMP

(FINISH)
@value
D=M
@R2
M=D

(END)
@END
0;JMP
