load Sum1toN.asm,
output-file Sum1toN.out,
// compare-to Sum1toN.cmp,
output-list RAM[0]%D2.6.2 RAM[1]%D2.6.2;

set RAM[0] 3,
repeat 100 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 4,
repeat 100 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 5,
repeat 100 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 6,
repeat 200 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 7,
repeat 200 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 8,
repeat 200 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 9,
repeat 200 {
  ticktock;
}
output;

set PC 0,
set RAM[0] 10,
repeat 200 {
  ticktock;
}
output;
