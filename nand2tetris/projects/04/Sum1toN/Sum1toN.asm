// Setup
    // Store @R0 as @limit.
    @R0
    D=M
    @limit
    M=D

    // Initialize @sum = 0.
    @sum
    M=0

    // Initialize @count = 0.
    @count
    M=0

(LOOP)
    // Increment counter.
    @count
    M=M+1

    // Add counter to @sum.
    D=M
    @sum
    M=M+D

    // Check if iteration limit has been reached.
    @limit
    D=M
    @count
    D=D-M

    // If iteration limit has been reached, jump to finalization.
    @FINALIZE
    D;JEQ

    // Jump to start of loop.
    @LOOP
    0;JMP

(FINALIZE)
    // Store final sum in @R1.
    @sum
    D=M
    @R1
    M=D

(END)
    @END
    0;JMP
