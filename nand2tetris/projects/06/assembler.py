from collections import OrderedDict
import os
from pathlib import Path
import re
import sys


def get_file_lines(filename):
    lines = None
    with open(filename) as f:
        lines = f.readlines()
        lines = [x.strip() for x in lines]
    return lines


class CodeLine:
    LABEL_PARSER = re.compile(r'^\((?P<label>.*)\)$')

    def __init__(self, instruction_number, code):
        self.instruction_number = instruction_number
        self.code = code
        self.is_label = self.is_label_string(code)

    @property
    def label(self):
        return self.extract_label(self.code) if self.is_label else None

    @classmethod
    def is_label_string(cls, input):
        return bool(cls.LABEL_PARSER.match(input))

    @classmethod
    def extract_label(cls, label_string):
        result = None
        m = cls.LABEL_PARSER.match(label_string)
        if m:
            result = m.group('label')
        return result

    def __str__(self):
        return '<CodeLine {:5d} {}: {:7s}>'.format(self.instruction_number, self.is_label, self.code)

    __repr__ = __str__

class Assembler:
    LINE_PARSER = re.compile(r'^(?P<code>.*?)(\s*?)(//(?P<comment>.*))*$')
    ADDRESS_PARSER = re.compile(r'^@(?P<address>.*)$')
    NUMBER_PARSER = re.compile(r'^\d+$')
    INSTRUCTION_PARSER = re.compile(r'^((?P<dest>.*)=)*(?P<comp>[^;]*)(;(?P<jump>.*))*$')
    LABEL_PARSER = re.compile(r'^\((?P<label>.*)\)$')

    MEMORY_PREDEFINED_SYMBOLS = {
        'R0': 0,
        'R1': 1,
        'R2': 2,
        'R3': 3,
        'R4': 4,
        'R5': 5,
        'R6': 6,
        'R7': 7,
        'R8': 8,
        'R9': 9,
        'R10': 10,
        'R11': 11,
        'R12': 12,
        'R13': 13,
        'R14': 14,
        'R15': 15,
        'SP': 0,
        'LCL': 1,
        'ARG': 2,
        'THIS': 3,
        'THAT': 4,
        'SCREEN': 16384,
        'KBD': 24576,
    }

    COMPUTE_BIT_PATTERNS = {
        '0':   '101010',
        '1':   '111111',
        '-1':  '111010',
        'D':   '001100',
        'A':   '110000',
        '!D':  '001101',
        '!A':  '110001',
        '-D':  '001111',
        '-A':  '110011',
        'D+1': '011111',
        'A+1': '110111',
        'D-1': '001110',
        'A-1': '110010',
        'D+A': '000010',
        'D-A': '010011',
        'A-D': '000111',
        'D&A': '000000',
        'D|A': '010101',
    }

    COMPUTE_LOOKUP_PATTERNS = {
        '0':   ('0', 0),
        '1':   ('1', 0),
        '-1':  ('-1', 0),
        'D':   ('D', 0),
        'A':   ('A', 0),
        '!D':  ('!D', 0),
        '!A':  ('!A', 0),
        '-D':  ('-D', 0),
        '-A':  ('-A', 0),
        'D+1': ('D+1', 0),
        '1+D': ('D+1', 0),
        'A+1': ('A+1', 0),
        '1+A': ('A+1', 0),
        'D-1': ('D-1', 0),
        'A-1': ('A-1', 0),
        'D+A': ('D+A', 0),
        'A+D': ('D+A', 0),
        'D-A': ('D-A', 0),
        'A-D': ('A-D', 0),
        'D&A': ('D&A', 0),
        'A&D': ('D&A', 0),
        'D|A': ('D|A', 0),
        'A|D': ('D|A', 0),
        'M': ('A', 1),
        '!M': ('!A', 1),
        '-M': ('-A', 1),
        'M+1': ('A+1', 1),
        '1+M': ('A+1', 1),
        'M-1': ('A-1', 1),
        'D+M': ('D+A', 1),
        'M+D': ('D+A', 1),
        'D-M': ('D-A', 1),
        'M-D': ('A-D', 1),
        'D&M': ('D&A', 1),
        'M&D': ('D&A', 1),
        'D|M': ('D|A', 1),
        'M|D': ('D|A', 1),
    }

    MAP_BOOL_TO_STR = {
        True: '1',
        False: '0',
    }

    MAP_JUMP_SPEC = {
        None: '000',
        'JGT': '001',
        'JEQ': '010',
        'JGE': '011',
        'JLT': '100',
        'JNE': '101',
        'JLE': '110',
        'JMP': '111',
    }

    def __init__(self, filename):
        self.filename = filename
        self.lines = None
        self.code_lines = []
        self.machine_lines = []
        self.custom_address_symbols = {}
        self.label_addresses = OrderedDict()
        self.next_available_address_symbol = 16
        self.current_output_instruction = 0

        self.run()

    def run(self):
        self.loadfile()
        self.process_lines()
        self.assemble_lines()
        self.write_machine_file()

    def loadfile(self):
        self.lines = get_file_lines(self.filename)

    def write_machine_file(self):
        filebase = os.path.splitext(self.filename)[0]
        new_filename = filebase + '.hack'
        p = Path(self.filename).parent.joinpath(new_filename)
        print('Assembling: {}'.format(p.resolve()))
        with open(p.resolve(), 'w') as f:
            for l in self.machine_lines:
                f.write(l + '\n')

    def process_lines(self):
        instruction_number = 0
        for line in self.lines:
            if (line):
                m = self.LINE_PARSER.match(line)
                code = m.groupdict().get('code')
                if (code):
                    code_line = CodeLine(instruction_number, code)
                    if code_line.is_label:
                        self.label_addresses[code_line.label] = code_line.instruction_number
                    else:
                        instruction_number += 1
                        self.code_lines.append(code_line)

    @staticmethod
    def convert_to_binary(number):
        return '{0:016b}'.format(int(number))

    def translate_address(self, address):
        result = None
        if self.NUMBER_PARSER.match(address):
            result = address
        elif address in self.MEMORY_PREDEFINED_SYMBOLS:
            result = self.MEMORY_PREDEFINED_SYMBOLS[address]
        elif address in self.label_addresses:
            result = self.label_addresses[address]
        elif address in self.custom_address_symbols:
            result = self.custom_address_symbols[address]
        else:
            # First time seeing custom symbol.
            result = self.next_available_address_symbol
            self.custom_address_symbols[address] = result
            self.next_available_address_symbol += 1
        if result is not None:
            result = self.convert_to_binary(result)
        return result

    def translate_dest_spec(self, dest):
        def find_in_dest(search_key):
            return dest is not None and search_key in dest

        a_dest = find_in_dest('A')
        d_dest = find_in_dest('D')
        m_dest = find_in_dest('M')
        return ''.join([
            self.MAP_BOOL_TO_STR[x]
            for x
            in (a_dest, d_dest, m_dest)
        ])

    def translate_instruction(self, dest, comp, jump):
        comp_spec = self.COMPUTE_LOOKUP_PATTERNS[comp]
        comp_bit_pattern = self.COMPUTE_BIT_PATTERNS[comp_spec[0]]
        comp_pattern_result = '111' + str(comp_spec[1]) + comp_bit_pattern
        dest_result = self.translate_dest_spec(dest)
        jump_result = self.MAP_JUMP_SPEC[jump]
        result = comp_pattern_result + dest_result + jump_result

        return result

    def store_machine_instruction(self, machine_instruction):
        self.machine_lines.append(machine_instruction)
        self.current_output_instruction += 1

    def assemble_line(self, code_line):
        line = code_line.code
        m = self.ADDRESS_PARSER.match(line)
        if m:
            address = m.group('address')
            translated_address = self.translate_address(address)
            self.store_machine_instruction(translated_address)
        else:
            m = self.INSTRUCTION_PARSER.match(line)
            if m:
                translated_instruction = self.translate_instruction(**m.groupdict())
                self.store_machine_instruction(translated_instruction)

    def assemble_lines(self):
        for code_line in self.code_lines:
            self.assemble_line(code_line)

if __name__ == '__main__':
    filename = sys.argv[1]
    asm = Assembler(filename)
